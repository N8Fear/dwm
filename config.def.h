/* See LICENSE file for copyright and license details. */

#include <X11/XF86keysym.h>

/* Solarized colors */
#define BASE03    "#002B36"
#define BASE02    "#073642"
#define BASE01    "#586E75"
#define BASE00    "#657B83"
#define BASE0     "#839496"
#define BASE1     "#93A1A1"
#define BASE2     "#EEE8D5"
#define BASE3     "#FDF6E3"
#define YELLOW    "#B58900"
#define ORANGE    "#CB4B16"
#define RED       "#DC322F"
#define MAGENTA   "#D33682"
#define VIOLET    "#6C71C4"
#define BLUE      "#268BD2"
#define CYAN      "#2AA198"
#define GREEN     "#859900"


/* appearance */
static const char *fonts[] = {
    "Ubuntu:size=10:bold",
    "Droid Sans:size=10",
};

/* color stuff */

static const char color[NUMCOL][ColLast][8] = {
    /* border foreground background */
    { VIOLET , BASE03, BASE1,  MAGENTA }, /* 0 = unselected, unoccupied */
    { BLUE  , BASE02, BLUE,    MAGENTA }, /* 1 = selected, occupied */
    { RED	 , BASE03, RED,    MAGENTA }, /* 2 = urgent */
    { GREEN	 , BASE03, YELLOW, MAGENTA }, /* 3 = unselected, occupied */
    { BLUE	 , BASE03, ORANGE, MAGENTA }, /* 4 = selected, unoccupied */
};

static const char dmenufont[] = "Ubuntu:size=10:bold";
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */


#define SHOW_LAYOUT_SYMBOL	0
#define SHOW_WINDOW_NAME	0

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
    { "Firefox", "Navigator", NULL,       1 << 3,       0,            0 },
    { "Chromium", NULL,       NULL,       1 << 2,       0,           -1 },
    { "Pidgin",   NULL,       NULL,       1 << 1,       0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-fn", dmenufont, "-nb", color[0][ColBG], "-nf", color[0][ColFG], "-sb", color[1][ColBG], "-sf", color[1][ColFG], NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *surfcmd[]  = { "firefox", NULL };
static const char *lockcmd[]  = { "slock", NULL };
static const char *volmutecmd[] = { "amixer", "sset", "Master", "toggle", NULL };
static const char *volupcmd[]   = { "amixer", "sset", "Master", "3+", NULL };
static const char *voldowncmd[] = { "amixer", "sset", "Master", "3-", NULL };
static const char *brightnessdowncmd[] = { "xbacklight", "-dec", "5", NULL };
static const char *brightnessupcmd[] = { "xbacklight", "-inc", "5", NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */
    { MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
    { MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
    { MODKEY|ShiftMask,             XK_f,      spawn,          {.v = surfcmd } },
    { MODKEY|ShiftMask,             XK_l,      spawn,          {.v = lockcmd } },
    { 0,                 XF86XK_AudioMute,     spawn,          {.v = volmutecmd } },
    { 0,          XF86XK_AudioLowerVolume,     spawn,          {.v = voldowncmd } },
    { 0,          XF86XK_AudioRaiseVolume,     spawn,          {.v = volupcmd   } },
    { 0,          XF86XK_MonBrightnessUp ,     spawn,          {.v = brightnessupcmd } },
    { 0,          XF86XK_MonBrightnessDown,    spawn,          {.v = brightnessdowncmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, swap,           {0} },
	{ MODKEY,                       XK_m,      setmark,        {.i = 1} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
/*
 * Disable Monocle
 * { MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
 */
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ 0 } /* pedantic breaks at empty initializer */
/* Disable mouse click support
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
*/
};

