# dwm version
VERSION = 6.1

# Customize below to fit your system

# pkg-config
PKG_CONFIG ?= pkg-config

# paths
PREFIX = /usr
MANPREFIX = ${PREFIX}/share/man

X11INC = $(shell $(PKG_CONFIG) --cflags x11)
X11LIBS = $(shell $(PKG_CONFIG) --libs-only-l x11)

FTINC = $(shell $(PKG_CONFIG) --cflags xft )
FTLIBS = $(shell $(PKG_CONFIG) --libs-only-l xft fontconfig)
#
# Xinerama, comment if you don't want it
XINERAMALIBS  = $(shell $(PKG_CONFIG) --cflags xinerama )
XINERAMAFLAGS = $(shell $(PKG_CONFIG) --libs xinerama )

INCS = ${X11INC} ${FTINC}
LIBS = ${FTLIBS} ${X11LIBS} ${XINERAMALIBS}

# flags
CPPFLAGS += -DVERSION=\"$(VERSION)\" -D_DEFAULT_SOURCE -D_POSIX_SOURCE=2
CFLAGS += -std=c99 -pedantic -Wall $(INCS) $(CPPFLAGS)
LDFLAGS += -s $(LIBS)

# compiler and linker
CC ?= gcc
